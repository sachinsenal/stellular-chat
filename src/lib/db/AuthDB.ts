/**
 * @file Create Auth DB
 * @name AuthDB.ts
 * @license MIT
 */

import { CreateHash } from "./helpers/Hash";
import SQL, { Config } from "./helpers/SQL";

import { Database } from "bun:sqlite";
import type { Pool } from "pg";

import translations from "./objects/translations.json";

import CommunicationDB, { type Group } from "./CommunicationDB";

import {
    type GetObjectCommandOutput,
    type PutObjectCommandOutput,
    type DeleteObjectCommandOutput,
    PutObjectCommand,
    GetObjectCommand,
    DeleteObjectCommand,
} from "@aws-sdk/client-s3";
import { comms, S3Client } from "$lib/server_stores";

// types
export type AuthState = {
    Joined: number;
    ID: string;
    Username: string;
    DisplayName: string;
    About: string;
    $metadata: {};
    UnhashedID?: string;
};

// ...

/**
 * @export
 * @class AuthDB
 */
export default class AuthDB {
    public static isNew: boolean = true;
    public readonly db: Pool | Database;

    // limits
    public static readonly MinUsernameLength = 4;
    public static readonly MaxUsernameLength = 32;
    private static readonly UsernameRegex = /^[\w\_\-\.\!]+$/gm;

    /**
     * Creates an instance of AuthDB.
     * @memberof AuthDB
     */
    constructor() {
        // create db link
        const [db, isNew] = Config.use_pg
            ? [
                  SQL.CreatePostgres(
                      Config.pg_host,
                      Config.pg_user,
                      Config.pg_password,
                      Config.pg_database
                  ),
                  false,
              ]
            : SQL.CreateDB("auth", Config.data_directory);

        AuthDB.isNew = isNew;
        this.db = db;

        (async () => {
            await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
                // @ts-ignore
                db,
                query: `CREATE TABLE IF NOT EXISTS "Users" (
                    "Joined" float,
                    "ID" varchar(256),
                    "Username" varchar(${AuthDB.MaxUsernameLength}),
                    "DisplayName" varchar(${AuthDB.MaxUsernameLength}),
                    "About" varchar(${CommunicationDB.MaxMessageLength}),
                    "$metadata" varchar(400000)
                )`,
            });

            // make sure direct messages galaxy exists
            const dms = await comms.GetGroupByID("@me");

            if (!dms[2])
                await comms.CreateGroup({
                    Name: "direct_messages",
                    ID: "@me",
                    Owner: "system",
                    Invite: "_no_invite", // don't allow this galaxy to be joined with an invite
                    $metadata: {} as Group["$metadata"],
                });
        })();
    }

    // get

    /**
     * @method GetUserFromID
     * @description Pull a user record by "ID" field
     *
     * @param {string} userid
     * @return {Promise<[boolean, string, AuthState?]>}
     * @memberof AuthDB
     */
    public async GetUserFromID(
        userid: string
    ): Promise<[boolean, string, AuthState?]> {
        // attempt to get user
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Users" WHERE "ID" = ?',
            params: [userid.toLowerCase()],
            get: true,
            use: "Query",
        })) as AuthState;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.user_exists, record];
        else return [false, translations.English.error_not_found];
    }

    /**
     * @method GetUserFromName
     * @description Pull a user record by "Username" field
     *
     * @param {string} username
     * @return {Promise<[boolean, string, AuthState?]>}
     * @memberof AuthDB
     */
    public async GetUserFromName(
        username: string
    ): Promise<[boolean, string, AuthState?]> {
        // attempt to get user
        const record = (await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'SELECT * FROM "Users" WHERE "Username" = ?',
            params: [username.toLowerCase()],
            get: true,
            use: "Query",
        })) as AuthState;

        if (record && typeof record.$metadata === "string")
            record.$metadata = JSON.parse(record.$metadata);

        // return
        if (record) return [true, translations.English.user_exists, record];
        else return [false, translations.English.error_not_found];
    }

    // set

    /**
     * @method CreateUser
     * @description Create a new user
     *
     * @param {string} username
     * @return {Promise<[boolean, string, AuthState?]>}
     * @memberof AuthDB
     */
    public async CreateUser(
        username: string
    ): Promise<[boolean, string, AuthState?]> {
        // make sure a user with this name doesn't already exist
        const user = await this.GetUserFromName(username);
        if (user[0]) return [false, translations.English.error_username_unavailable];

        // check username
        if (
            username.length < AuthDB.MinUsernameLength ||
            username.length > AuthDB.MaxUsernameLength
        )
            return [false, translations.English.error_username_length];

        if (!username.match(AuthDB.UsernameRegex))
            return [false, `Username does not pass test: ${AuthDB.UsernameRegex}`];

        // create user
        const uuid = crypto.randomUUID();
        const hashed_uuid = CreateHash(uuid); // user ids are essentially passwords, so they should be hashed
        // the server doesn't need the source of the hash, so the hashed uuid works as an id!

        const time = new Date().getTime();

        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'INSERT INTO "Users" VALUES (?, ?, ?, ?, ?, ?)',
            params: [
                time, // Joined
                hashed_uuid, // ID
                username, // Username
                username, // DisplayName
                "", // About
                "{}", // $metadata
            ],
            transaction: true,
            use: "Prepare",
        });

        // return
        return [
            true,
            translations.English.user_created,
            {
                Username: username,
                DisplayName: username,
                ID: hashed_uuid,
                Joined: time,
                About: "",
                $metadata: {
                    picture: "",
                },
                UnhashedID: uuid,
            },
        ];
    }

    /**
     * @method SetUserAbout
     * @description Change the "About" field of a user record
     *
     * @param {string} id User ID
     * @param {string} about New "About" content
     * @return {Promise<[boolean, string, AuthState?]>}
     * @memberof AuthDB
     */
    public async SetUserAbout(
        id: string,
        about: string
    ): Promise<[boolean, string, AuthState?]> {
        // make sure user exists
        const user = await this.GetUserFromID(id);
        if (!user[0] || !user[2]) return [false, user[1]];

        // validate input
        if (
            about.length < CommunicationDB.MinMessageLength ||
            about.length > CommunicationDB.MaxMessageLength
        )
            return [false, translations.English.error_message_length];

        // update record
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'UPDATE "Users" SET ("About") = (?) WHERE "ID" = ?',
            params: [about, id],
            transaction: true,
            use: "Prepare",
        });

        // return
        user[2].About = about;
        return [true, translations.English.user_updated, user[2]];
    }

    /**
     * @method SetUserDisplayName
     * @description Change the "DisplayName" field of a user record
     *
     * @param {string} id User ID
     * @param {string} displayname New DisplayName
     * @return {Promise<[boolean, string, AuthState?]>}
     * @memberof AuthDB
     */
    public async SetUserDisplayName(
        id: string,
        displayname: string
    ): Promise<[boolean, string, AuthState?]> {
        // make sure user exists
        const user = await this.GetUserFromID(id);
        if (!user[0] || !user[2]) return [false, user[1]];

        // validate input
        if (
            displayname.length < AuthDB.MinUsernameLength ||
            displayname.length > AuthDB.MaxUsernameLength
        )
            return [false, translations.English.error_name_length];

        // update record
        await (Config.use_pg ? SQL.PostgresQueryOBJ : SQL.QueryOBJ)({
            // @ts-ignore
            db: this.db,
            query: 'UPDATE "Users" SET ("DisplayName") = (?) WHERE "ID" = ?',
            params: [displayname, id],
            transaction: true,
            use: "Prepare",
        });

        // return
        user[2].DisplayName = displayname;
        return [true, translations.English.user_updated, user[2]];
    }

    // PROFILE PICTURE

    // get

    /**
     * @method GetProfilePicture
     *
     * @param {string} username
     * @return {Promise<GetObjectCommandOutput>}
     * @memberof CommunicationDB
     */
    public async GetProfilePicture(
        username: string
    ): Promise<GetObjectCommandOutput> {
        const name = `avatars/${username}`;

        const res = await S3Client.send(
            new GetObjectCommand({
                Bucket: "stellular",

                // file information
                Key: name,
            })
        );

        return res;
    }

    // set

    /**
     * @methhod UploadProfilePicture
     *
     * @param {string} username
     * @param {File} attachment
     * @return {Promise<[string, PutObjectCommandOutput] | [boolean, string]>} [key, PutObjectCommandOutput] | [success, message]
     * @memberof CommunicationDB
     */
    public async UploadProfilePicture(
        username: string,
        attachment: File
    ): Promise<[string, PutObjectCommandOutput] | [boolean, string]> {
        if (attachment.size > 15000000)
            return [false, translations.English.error_file_too_large];

        if (!attachment.type.startsWith("image/"))
            return [false, translations.English.error_file_type];

        const name = `avatars/${username}`;

        const res = await S3Client.send(
            new PutObjectCommand({
                Bucket: "stellular",
                ServerSideEncryption: "AES256",

                // file information
                Key: name,
                // @ts-ignore don't let the error lie to you, it wants an ArrayBuffer
                Body: await attachment.arrayBuffer(),
                ContentType: attachment.type,
            })
        );

        return [name, res];
    }

    // delete

    /**
     * @methhod DeleteProfilePhoto
     *
     * @param {string} username
     * @return {Promise<DeleteObjectCommandOutput>}
     * @memberof CommunicationDB
     */
    public async DeleteProfilePhoto(
        username: string
    ): Promise<DeleteObjectCommandOutput> {
        const name = `avatars/${username}`;

        const res = await S3Client.send(
            new DeleteObjectCommand({
                Bucket: "stellular",

                // file information
                Key: name,
            })
        );

        return res;
    }
}
