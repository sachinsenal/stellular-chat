/**
 * @file Handle permission values and utilities
 * @name Permissions.ts
 * @license MIT
 */

export enum MEMBERSHIP_STATUS {
    NOT_MEMBER = -1,
    BANNED = 0,
    MEMBER = 1,
    PROTECTED = 1.5, // protected members who cannot be banned or kicked
    STAFF = 2,
    OWNER = 5,
}

export enum MINIMUM_ACTION_REQUIREMENTS {
    JOIN_GALAXY = MEMBERSHIP_STATUS.NOT_MEMBER,
    VIEW_INVITE = MEMBERSHIP_STATUS.STAFF,
    SEND_MESSAGES = MEMBERSHIP_STATUS.MEMBER,
    MANAGE_PLANETS = MEMBERSHIP_STATUS.OWNER,
    MANAGE_USERS = MEMBERSHIP_STATUS.OWNER,
    MANAGE_MESSAGES = MEMBERSHIP_STATUS.STAFF,
}

// default export
export default { MEMBERSHIP_STATUS, MINIMUM_ACTION_REQUIREMENTS };
