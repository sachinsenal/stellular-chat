/**
 * @file Utilities for handling HTTP requests
 * @name RequestUtilities.ts
 * @license MIT
 */

/**
 * @function VerifyContentType
 *
 * @export
 * @param {Request} request
 * @param {string} expected
 * @return {(Response | undefined)}
 */
export function VerifyContentType(
    request: Request,
    expected: string
): Response | undefined {
    // verify content type
    if (!(request.headers.get("Content-Type") || "").startsWith(expected))
        return new Response(
            JSON.stringify({
                success: false,
                message: `Expected ${expected}`,
                payload: [false, `Expected ${expected}`],
            }),
            {
                status: 406,
                headers: {
                    Accept: expected,
                    "Content-Type": "application/json",
                },
            }
        );

    // return undefined if request is fine
    return undefined;
}

// default export
export default {
    VerifyContentType,
};
