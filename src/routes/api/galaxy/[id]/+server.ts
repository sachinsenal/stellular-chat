import type { RequestHandler } from "./$types";
import { json, error } from "@sveltejs/kit";

import { MEMBERSHIP_STATUS } from "$lib/db/CommunicationDB";
import { auth, comms } from "$lib/server_stores";

export const GET: RequestHandler = async (props) => {
    // get channel and return
    const res = await comms.GetGroupByID(props.params.id);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};

export const DELETE: RequestHandler = async (props) => {
    // get userid from cookie
    const cookie = props.cookies.get("__Secure-StarID");

    // check expected values
    if (!cookie)
        return json(
            {
                success: false,
                message: "Missing cookie",
            },
            { status: 400 }
        );

    // get user
    const user = await auth.GetUserFromID(cookie);
    if (!user[0] || !user[2]) error(401);

    // check user membership in galaxy
    const membership = await comms.GetUserGroupMembership(
        user[2].Username,
        props.params.id
    );

    if (!membership[0] || membership[2] < MEMBERSHIP_STATUS.OWNER)
        return error(401, "Unauthorized"); // user MUST be AT LEAST an owner

    // delete galaxy and return
    const res = await comms.DeleteGroup(props.params.id);

    return json(
        {
            success: res[0],
            message: res[1],
            payload: res[2],
        },
        {
            status: res[0] === true ? 200 : 400,
        }
    );
};
