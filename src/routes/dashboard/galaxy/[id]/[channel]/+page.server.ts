import type { PageServerLoad } from "./$types";
import { error } from "@sveltejs/kit";

import { comms, auth } from "$lib/server_stores";

export const load: PageServerLoad = async ({ cookies, params, url }) => {
    let data: { [key: string]: any } = {};

    // get auth cookie
    const cookie = cookies.get("__Secure-StarID");

    if (cookie) {
        // get user
        const user = await auth.GetUserFromID(cookie);
        if (!user[0] || !user[2]) return error(401, "Session Invalid");
        data.user = user[2];

        // get current galaxy
        const galaxy = await comms.GetGroupByID(params.id);
        if (!galaxy[0] || !galaxy[2]) return error(404, galaxy[1]);
        data.galaxy = galaxy;

        // get current planet
        const planet = await comms.GetChannelByID(params.channel);
        if (!planet[0] || !planet[2]) return error(404, planet[1]);
        data.planet = planet;

        // if we're looking at a direct message channel, replace planet name with the name of the other user!
        if (galaxy[2].ID === "@me") {
            const otherUserID = data.planet[2].ID.split("@direct:")[1]
                .replace(user[2].ID, "")
                .replace(":", "");

            // get other user
            const otherUser = await auth.GetUserFromID(otherUserID);
            if (!otherUser[0] || !otherUser[2]) return error(404, otherUser[1]);
            data.planet[2].Name = otherUser[2].Username;
        }

        // get planet messages
        const messages = await comms.GetChannelMessages(
            params.channel,
            200,
            parseInt(url.searchParams.get("offset") || "0"),
            planet[2].Type === "text" ? "ASC" : "DESC"
        );

        if (!messages[0] || !messages[2]) return error(404, messages[1]);
        data.messages = messages;

        // channels can be viewed by anybody by default, however you cannot post without being a member of the galaxy!
        const membership = await comms.GetUserGroupMembership(
            user[2].Username,
            params.id
        );

        data.membership = membership[2];

        // get planets
        const planets =
            galaxy[2].ID !== "@me"
                ? await comms.GetGroupChannels(galaxy[2].ID)
                : await comms.GetGroupChannels("@me", user[2].ID);

        if (!planets[0] || !planets[2]) return error(404);
        data.planets = planets;

        // if we're looking at direct messages, set planet name to the name of the OTHER USER
        if (galaxy[2].ID === "@me")
            for (const planet of data.planets[2]) {
                const otherUserID = planet.ID.split("@direct:")[1]
                    .replace(user[2].ID, "")
                    .replace(":", "");

                // get other user
                const otherUser = await auth.GetUserFromID(otherUserID);
                if (!otherUser[0] || !otherUser[2]) continue;
                planet.Name = otherUser[2].Username;
            }
    }

    if (!data.planet) return error(401, "Unauthorized");

    // return
    return data;
};
