import adapter from "@sveltejs/adapter-node";
import { vitePreprocess } from "@sveltejs/vite-plugin-svelte";

// import { spawnSync } from "bun";
import { execSync } from "node:child_process";

/** @type {import('@sveltejs/kit').Config} */
const config = {
    // Consult https://kit.svelte.dev/docs/integrations#preprocessors
    // for more information about preprocessors
    preprocess: vitePreprocess(),

    kit: {
        adapter: adapter(),
        version: {
            // name: spawnSync("git describe --tags --long --always".split(" "))
            //     .stdout.toString()
            //     .trim(),
            name: execSync("git describe --tags --long --always").toString().trim(),
        },
    },
};

export default config;
