# ☄️ stellular

## Configuration

Stellular is configured through environment variables. Below is a list of all configuration options and what they do:

- `data_directory`: The directory your data is contained in (default: `(cwd)/data`)
- `use_pg`: Enables you to use PostgreSQL instead of SQLite (default: `false`)
    - `pg_host`: Database host (default: `localhost`)
    - `pg_user`: Database user (default: `postgres`)
    - `pg_password`: Database user password (default: `postgres`)
    - `pg_database`: Database name (default: `default`)
    - `pg_max_clients`: (number), Maximum number of clients connected to the PostgreSQL server (default: `10`)

Stellular uses S3 compatible storage for file storage via `@aws-sdk/client-s3`. You can provide the configuration options for this with the following environment variables:

- `s3_region`: (default: `auto`)
- `s3_endpoint`: Required
- `s3_accessKeyId`: Required
- `s3_secretAccessKey`: Required
- `s3_public_endpoint`: Required

Your S3 bucket should simply be named "stellular"

## API

- `POST /api/auth/new`, Create a new account
    - Accepts `application/json`
    - Expects body with: `Username(string)`
- `POST /api/auth/login`, Sign into an existing account with its ID
    - Accepts `application/json`
    - Expects body with: `UserID(string)`
- `POST /api/auth/logout`, Clear authentication cookie
    - Expects cookie `__Secure-StarID`
- `POST /api/star/[id]/update`, Update user profile "About" field
    - Expects cookie `__Secure-StarID`
    - Accepts `application/json`
    - Expects body with: `Content(string)`
- `POST /api/star/[id]/photo`, Update user profile profile picture
    - Expects cookie `__Secure-StarID`
    - Accepts `multipart/form-data`
    - Expects body with: `photo(File)`
- `POST /api/star/[id]/displayname`, Update user profile "DisplayName" field
    - Expects cookie `__Secure-StarID`
    - Accepts `application/json`
    - Expects body with: `DisplayName(string)`
- `POST /api/galaxy/create`, Create galaxy
    - Accepts `application/json`
    - Expects cookie `__Secure-StarID`
    - Expects body with: `Name(string)`
- `GET /api/galaxy/[id]`, Get a specific galaxy
- `POST /api/galaxy/[id]/planets`, Create new planet in galaxy
    - Accepts `application/json`
    - Expects cookie `__Secure-StarID`
    - Expects body with: `Name(string)`
        - Optionally accepts body with: `User(string)`, this will convert the channel to a direct message with the specified user (This only applies in the `@me` galaxy)
        - Optionally accepts body with: `Type("feed" | "text")`
- `DELETE /api/galaxy/[id]`, Delete a specific galaxy
    - Expects cookie `__Secure-StarID`
- `GET /api/galaxy/[id]/planets`, Get all planets in a galaxy
- `GET /api/galaxy/[id]/planets/[channel]`, Get a specific planet in a galaxy
- `DELETE /api/galaxy/[id]/plannets/[channel]`, Delete a specific channel
    - Expects cookie `__Secure-StarID`
- `POST /api/galaxy/[id]/planets/[channel]/messages`, Create new message/post in planet
    - Accepts `multipart/form-data`
    - Expects cookie `__Secure-StarID`
    - Expects body with: `Content(string)`
        - Optionally accepts body with: `Content(string), Reply(string), attachments(File[])`
- `GET /api/galaxy/[id]/planets/[channel]/messages`, Get all messages in a planet
- `GET /api/galaxy/[id]/planets/[channel]/messages/[message]`, Get a specific message in a planet
- `PUT /api/galaxy/[id]/plannets/[channel]/messages/[message]`, Edit a specific message
    - Accepts `multipart/form-data`
    - Expects cookie `__Secure-StarID`
    - Expects body with: `Content(string)`
- `DELETE /api/galaxy/[id]/plannets/[channel]/messages/[message]`, Delete a specific message
    - Expects cookie `__Secure-StarID`
- `POST /api/galaxy/invite/[id]`, Join a galaxy from its invite code
    - Expects cookie `__Secure-StarID`

All API endpoints (including `GET`) return a structure similar to the following:

```ts
type APIResponse = {
    success: boolean;
    message: string;
    payload: any; // this depends on the API route, and will likely return undefined when success is false
}
```
